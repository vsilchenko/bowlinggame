#ifndef _BOWLING_GAME_10_PIN_H_
#define _BOWLING_GAME_10_PIN_H_

#include <queue>

/// @brief Represents  single 10 pin bowling game
///
/// Normal sequence is to construct object and then call RecordBall and any of Getters until IsComplete is true MoveToNextFrame
/// returns false
class BowlingGame10Pin
{
public:
    BowlingGame10Pin();

    /// @brief Checks if game is complete
    ///
    /// No updates are allowed after the game is complete
    ///
    /// @return True if game is complete, false otherwise
    bool IsComplete() const;

    /// @brief Returns current frame number (0 for the first frame)
    int GetCurrentFrameNum() const;

    /// @brief Returns current frame ball number (0 for the first ball, 1 for the second)
    int GetCurrentFrameBallNum() const;

    /// @brief Returns current score
    int GetCurrentScore() const;

    /// @brief Records new ball. Moves to the next frame if has to.
    int RecordBall(int pins);

private:
    void _moveToNextBall();
    int _calculateScore();
    int _getBallScore(unsigned int ballNum) const;

    int _ballsLeft;
    int _currentFrame;
    int _currentBallInFrame;
    bool _bonusBalls;
    int _currentScore;

    std::vector<int> _playedBalls;
};

#endif

