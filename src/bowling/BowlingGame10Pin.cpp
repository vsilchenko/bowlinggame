#include "bowling/BowlingGame10Pin.h"

namespace
{
    const int STRIKE_PINS = 10;
    const int UNDEFINED_VALUE = -1;
}

BowlingGame10Pin::BowlingGame10Pin():
    _ballsLeft(20),
    _currentFrame(0),
    _currentBallInFrame(0),
    _bonusBalls(false),
    _currentScore(0)
{
}

bool
BowlingGame10Pin::IsComplete() const
{
    return _ballsLeft <= 0 && _bonusBalls;

}

int
BowlingGame10Pin::GetCurrentFrameNum() const
{
    return _currentFrame;
}

int
BowlingGame10Pin::GetCurrentFrameBallNum() const
{
    return _currentBallInFrame;
}

int
BowlingGame10Pin::GetCurrentScore() const
{
    return _currentScore;
}

int
BowlingGame10Pin::RecordBall(int pins)
{
    if (IsComplete())
    {
        return UNDEFINED_VALUE;
    }

    // validate provided pins
    int prevBallPins = _getBallScore(_playedBalls.size() - 1);
    if (pins > STRIKE_PINS)
    {
        // can't be bigger than strike
        return UNDEFINED_VALUE;
    }

    // TODO: Frame pins number shall be <= 10

    if (!_playedBalls.empty())
    {
        _moveToNextBall();
    }

    _playedBalls.push_back(pins);

    // re-calculate number of balls left
    _ballsLeft -= pins == STRIKE_PINS && !_bonusBalls ? 2 : 1;

    // organize bonus balls if have to
    if (_ballsLeft <= 0 && !_bonusBalls)
    {
        int lastBall = pins;
        int prevBall = _getBallScore(_playedBalls.size() - 2);
        _bonusBalls = true;

        if (lastBall == STRIKE_PINS)
        {
            _ballsLeft += 2;
        }
        else if (lastBall + prevBall == STRIKE_PINS)
        {
            _ballsLeft += 1;
        }
    }

    _calculateScore();

    return GetCurrentScore();
}

void
BowlingGame10Pin::_moveToNextBall()
{
    int prevPins = _playedBalls.size() >= 1 ? _playedBalls[_playedBalls.size() - 1] : UNDEFINED_VALUE;
    int prevPrevPins = _playedBalls.size() >= 2 ? _playedBalls[_playedBalls.size() - 2] : UNDEFINED_VALUE;


    // if last pins is strike - move to next frame
    if (prevPins == STRIKE_PINS && !_bonusBalls)
    {
        _currentFrame++;
        _currentBallInFrame = 0;
    }
    else if (!_bonusBalls && prevPrevPins != STRIKE_PINS && prevPrevPins != UNDEFINED_VALUE && _currentBallInFrame != 0)
    {
        // if last two balls are not strikes - move to next frame
        _currentFrame++;
        _currentBallInFrame = 0;
    }
    else
    {
        // just move to the second ball in the frame
        _currentBallInFrame++;
    }
}

int
BowlingGame10Pin::_calculateScore()
{
    int resultScore = 0;

    // Number of balls to calculate. Need to keep track to do not use bonus balls.
    int ballsLeft = 20;

    for (unsigned int i = 0; ballsLeft && i < _playedBalls.size(); i++)
    {
        int currentBallPins = _playedBalls[i];
        int nextBallPins = _getBallScore(i + 1);
        int afterNextBallPins = _getBallScore(i + 2);

        resultScore += currentBallPins;

        // if strike - add next balls as a bonus
        if (currentBallPins == STRIKE_PINS)
        {
            ballsLeft -= 2;
            resultScore += nextBallPins + afterNextBallPins;
        }
        else if (currentBallPins + nextBallPins == STRIKE_PINS)
        {
            // if spare - add next ball as a bonus
            ballsLeft -= 2;
            resultScore += nextBallPins + afterNextBallPins;
            i++;
        }
        else
        {
            ballsLeft--;
        }
    }

    _currentScore = resultScore;

    return resultScore;
}

int
BowlingGame10Pin::_getBallScore(unsigned int ballNum) const
{
    return ballNum < _playedBalls.size() ? _playedBalls[ballNum] : 0;
}
