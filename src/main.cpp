#include <iostream>

#include "bowling/BowlingGame10Pin.h"

int main(int, char**)
{
    BowlingGame10Pin game;

    int res = 0;
    do
    {
        int pins = 0;
        if (!(std::cin >> pins))
        {
            std::cerr << "Invalid number entered" << std::endl;
            res = -1;
            break;
        }

        if (game.RecordBall(pins) < 0)
        {
            std::cerr << "Invalid pins number entered" << std::endl;
            res = -1;
            break;
        }

        std::cout << game.GetCurrentFrameNum() << " " <<
                     game.GetCurrentFrameBallNum() << " " <<
                     game.GetCurrentScore() << std::endl;


    } while(!game.IsComplete());

    return res;
}
