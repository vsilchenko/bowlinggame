10 Pin Bowling Game Single Player Score Calculator
==================================================

The only purpose of this program is to keep score of a single-player 10 pin bowling game session.
Check http://en.wikipedia.org/wiki/Ten-pin_bowling#Scoring for more details about scoring rules.

Input requirements are:
- The program should be a console application taking input from standard input and printing state to standard output.
- Input will be the number of pins knocked down by the player in the current shot followed by a line feed.
- Output will be three space separated values. The current frame indexed from 0, the shot within the current frame 
    indexed from zero, and the current total score for the game.
- If invalid input is received the program should end with a non-zero return code.
- If the game is complete the program should end with a zero return code.

Also, since there was is no information available about the target environment, a couple of assumptions have been made:
- No extra output is allowed (i.e. no text like 'Enter number of pins' etc.), except for the errors.
- Unit tests library is not available.
- A gcc compiler is available.
- No build automation tools are available.
- No doxygen or any other documentation generation tool is available.

1. Known Issues
===========================
- Input data validation is limited to rudimentary boundary checks. More complicated cases would result into an invalid state, for example:
    - A user is able to enter more than 10 pins in a single frame, etc.

2. Development environment:
===========================
- Code has been developed and tested using gcc 4.8.3. However, since the code is not using any new fancy C++ 
    features o rlibraries - it should compile on older versions as well.
- Code has been developed on Windows machine. Cygwin has been used to test *nix environment build and run.
    There is a slight chance adjustments might be needed for the code to build and run on a real *nix machine.

3. Tests Build and Run:
=======================
- On Windows:
    - Run 'buildAndRunBowlingGameTests.bat' to compile and run the tests.
    
- On Linux:
    - Run 'sh buildAndRunBowlingGameTests.sh' to compile and start the tests.

If all tests pass, a user shall see 'Test result: Success', otherwise error description is printed.
    
4. Program Build and Run:
=========================
- On Windows:
    - Run 'buildAndRunBowlingGame.bat' to compile and start the program
    
- On Linux:
    - Run 'sh buildAndRunBowlingGame.sh' to compile and start the program
    
    
5. Room for Improvement:
========================
Since the main code and tests have been written in ~2 hours, there's some room for improvement:
- Better input validation, like number of pins in a frame has to be <= 10, etc.
- Proper build/run environment: either ant or maven have to be used to compile code, run unit tests and generate documentation.
- Proper unit testing: A proper unit testing library has to be used (googletest, for example). Unit tests have to cover all possible
    method calls combinations.
- Proper code documentation: every single class, method, method parameter and return result and instance variable has to be documented.
- Proper logic comments: due to strict time limits code might not be very self-descriptive. More comments would be handy.
- Code review and refactoring.