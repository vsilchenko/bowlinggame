#include "bowling/BowlingGame10Pin.h"

#include <iostream>

#define ASSERT_TRUE(x) \
    { \
        bool valueToTest = x; \
        if (!valueToTest) \
        { \
            std::cerr << "Error in " << __FUNCTION__ << ":" << __LINE__ << std::endl; \
            return false; \
        } \
    } \

bool
testInvalidInput()
{
    BowlingGame10Pin game;

    // numbes of pins shall not be more than a strike
    ASSERT_TRUE(game.RecordBall(11) == -1);

    return true;
}

bool
testNormalNonStrikeGame()
{
    BowlingGame10Pin game;

    // score is 0 by default
    ASSERT_TRUE(game.GetCurrentScore() == 0);

    // current frame/ball are both 0
    ASSERT_TRUE(game.GetCurrentFrameNum() == 0);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 0);

    // play first ball and check the score
    game.RecordBall(1);
    ASSERT_TRUE(game.GetCurrentFrameNum() == 0);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 0);
    ASSERT_TRUE(game.GetCurrentScore() == 1);

    // not complete yet
    ASSERT_TRUE(!game.IsComplete());


    // play second ball and check the score
    game.RecordBall(1);
    ASSERT_TRUE(game.GetCurrentFrameNum() == 0);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 1);
    ASSERT_TRUE(game.GetCurrentScore() == 2);

    // third ball should start new frame
    ASSERT_TRUE(game.RecordBall(2) != -1);
    ASSERT_TRUE(game.GetCurrentFrameNum() == 1);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 0);
    ASSERT_TRUE(game.GetCurrentScore() == 4);

    // second ball of the second frame ball should start new frame
    game.RecordBall(3);
    ASSERT_TRUE(game.GetCurrentFrameNum() == 1);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 1);
    ASSERT_TRUE(game.GetCurrentScore() == 7);

    // fill up the rest of the frame
    // frame 3
    ASSERT_TRUE(game.RecordBall(1) == 8);
    ASSERT_TRUE(game.RecordBall(1) == 9);


    // frame 4
    ASSERT_TRUE(game.RecordBall(1) == 10);
    ASSERT_TRUE(game.RecordBall(1) == 11);

    // frame 5
    ASSERT_TRUE(game.RecordBall(1) == 12);
    ASSERT_TRUE(game.RecordBall(1) == 13);

    // frame 6
    ASSERT_TRUE(game.RecordBall(1) == 14);
    ASSERT_TRUE(game.RecordBall(1) == 15);

    // frame 7
    ASSERT_TRUE(game.RecordBall(1) == 16);
    ASSERT_TRUE(game.RecordBall(1) == 17);

    // frame 8
    ASSERT_TRUE(game.RecordBall(1) == 18);
    ASSERT_TRUE(game.RecordBall(1) == 19);

    // frame 9
    ASSERT_TRUE(game.RecordBall(1) == 20);
    ASSERT_TRUE(game.RecordBall(1) == 21);

    // frame 10
    ASSERT_TRUE(game.RecordBall(1) == 22);
    ASSERT_TRUE(game.RecordBall(1) == 23);

    // at this point we should not be able to move any further
    // and we should be complete
    ASSERT_TRUE(game.IsComplete());

    // test final numbers
    ASSERT_TRUE(game.GetCurrentFrameNum() == 9);
    ASSERT_TRUE(game.GetCurrentFrameBallNum() == 1);
    ASSERT_TRUE(game.GetCurrentScore() == 23);

    return true;
}

// tests strikes scenarios from wiki
bool
testStrikes()
{
    BowlingGame10Pin game;
    ASSERT_TRUE(game.RecordBall(10) == 10);
    ASSERT_TRUE(game.GetCurrentScore() == 10);
    game.RecordBall(3);
    ASSERT_TRUE(game.GetCurrentScore() == 16);
    game.RecordBall(6);
    ASSERT_TRUE(game.GetCurrentScore() == 28);

    BowlingGame10Pin game2;
    game2.RecordBall(10);
    ASSERT_TRUE(game2.GetCurrentScore() == 10);
    game2.RecordBall(10);
    ASSERT_TRUE(game2.GetCurrentScore() == 30);
    game2.RecordBall(9);
    ASSERT_TRUE(game2.GetCurrentScore() == 57);
    game2.RecordBall(0);
    ASSERT_TRUE(game2.GetCurrentScore() == 57);


    return true;
}

bool
testSpares()
{
    BowlingGame10Pin game;
    game.RecordBall(7);
    ASSERT_TRUE(game.GetCurrentScore() == 7);
    game.RecordBall(3);
    ASSERT_TRUE(game.GetCurrentScore() == 10);
    game.RecordBall(4);
    ASSERT_TRUE(game.GetCurrentScore() == 18);
    game.RecordBall(2);
    ASSERT_TRUE(game.GetCurrentScore() == 20);

    return true;
}

bool
testBonus1Ball()
{
    BowlingGame10Pin game;

    // frame 1
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 2
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 3
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 4
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 5
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 6
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 7
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 8
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    ASSERT_TRUE(game.GetCurrentScore() == 210);
    // frame 9
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    ASSERT_TRUE(game.GetCurrentScore() == 240);
    // frame 10, first ball
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(7) != -1);
    ASSERT_TRUE(game.GetCurrentScore() == 261);
    // frame 10, second ball
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(3) != -1);
    ASSERT_TRUE(game.GetCurrentScore() == 267);
    // frame 10, bonus ball
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    ASSERT_TRUE(game.GetCurrentScore() == 277);

    // game's finished
    ASSERT_TRUE(game.GetCurrentFrameNum() == 9);
    ASSERT_TRUE(game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) == -1);

    return true;
}

bool
testAlLStrikes()
{
    BowlingGame10Pin game;

    // frame 1
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 2
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 3
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 4
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 5
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 6
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 7
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 8
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 9
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 10
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 10, first bonus ball
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);
    // frame 10, second bonus ball.
    ASSERT_TRUE(!game.IsComplete());
    ASSERT_TRUE(game.RecordBall(10) != -1);

    // finnaly - the game is complete
    ASSERT_TRUE(game.IsComplete());
    ASSERT_TRUE(game.GetCurrentFrameNum() == 9);

    ASSERT_TRUE(game.GetCurrentScore() == 300);

    return true;
}

int main(int, char**)
{
    bool success = testInvalidInput();
    success = success && testNormalNonStrikeGame();
    success = success && testStrikes();
    success = success && testSpares();
    success = success && testBonus1Ball();
    success = success && testAlLStrikes();

    std::cout << "Test result: " << (success ? "Succes" : "Error") << std::endl;

    return 0;
}
